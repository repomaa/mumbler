# Mumbler

An ansible playbook for setting up a group communication server

## Features

- Mumble with web interface
- Node media server for rtmp streaming with web player
- Jitsi Meet

## Prerequisites

Install ansible:

```
$> pip install ansible
```

Install requirements by running the following commands in this repository:

```
$> ansible-galaxy collection install -r requirements.yml
$> ansible-galaxy role install -r requirements.yml
```

## Config

Copy `vars/vars.skel.yml` to `vars/vars.yml` and `vars/secrets.skel.yml` to
`vars/secrets.yml` and configure to your liking. Set `setup_infrastructure` to
`false` if you don't want to use hetzner cloud to setup the server
infrastructure. Set `use_inwx` to `false` if you don't want ansible to set DNS
records for the domains on your server using the inwx api.

## Install

Make sure to set appropriate DNS records to your host if you don't use INWX.

If you use hcloud simply run:

```
$> ansible-playbook up.yml
```

If not, copy `inventory.skel` to `inventory`, set the ip to your host and run:

```
$> ansible-playbook -i inventory up.yml
```

## Usage

### Mumble

Connect your mumble client to your mumble domain or visit it with a browser for
the web client.

### Node Media Server

Setup OBS to stream to your nms domain. By setting up a custom server with the
url set to `rtmp://cast.example.com/live` and stream key to any random (or
non-random) string. Clients can now stream live by pointing their players to
`rtmp://cast.example.com/live/your_stream_key`. Alternatively they can visit
`https://cast.example.com/player#your_stream_key` to watch the stream from a
browser. This will have a lot of latency though.

### Jitsi meet

Simply go to https://your.jitsi.meet.domain and enjoy
